package number

import (
	"math/big"
)

//Nombre est une interface permettant de représenter n’importe quel nombre réel
//Ainsi que NaN et ±∞
type Number interface {
	Numerator() *big.Int
	Denominator() *big.Int
}

type nb struct {
	n, d *big.Int
}

func (n nb) Numerator() *big.Int   { return new(big.Int).Set(n.n) }
func (n nb) Denominator() *big.Int { return new(big.Int).Set(n.d) }

func (n nb) format() nb {
	n.n, n.d = Format(n)
	return n
}

//Retourne vrai si n est fini.
func IsNumber(n Number) bool { return n.Denominator().Sign() != 0 }

//SignOf retourne 1 si n > 0, 0 si n = 0 ou NaN et -1 sinon
func SignOf(n Number) int {
	sd, sn := n.Denominator().Sign(), n.Numerator().Sign()
	return sd * sn
}

func IsNegative(n Number) bool { return SignOf(n) < 0 }
func IsPositive(n Number) bool { return !IsNegative(n) }

func IsNan(n Number) bool              { return !IsNumber(n) && SignOf(n) == 0 }
func IsInfinite(n Number) bool         { return !IsNumber(n) && SignOf(n) != 0 }
func IsPositiveInfinite(n Number) bool { return !IsNumber(n) && SignOf(n) > 0 }
func IsNegativeInfinite(n Number) bool { return !IsNumber(n) && SignOf(n) < 0 }

func IsInteger(n Number) bool { return IsNumber(n) && Rational(n).IsInt() }
func IsZero(n Number) bool    { return IsNumber(n) && SignOf(n) == 0 }

//IsInt retourne vrai si le nombre est équivalent à l’entier spécifié
func IsInt(n Number, i int64) bool {
	if IsInteger(n) {
		ni := Int(n)
		return ni.IsInt64() && ni.Int64() == i
	}
	return false
}
func IsUint(n Number, i uint64) bool {
	if IsInteger(n) {
		ni := Int(n)
		return ni.IsUint64() && ni.Uint64() == i
	}
	return false
}
func Is(n Number, i int) bool {
	return IsInt(n, int64(i))
}

//IsEven retourne vrai si n est pair
func IsEven(n Number) bool { return IsInteger(n) && Int(n).Bit(0) == 0 }

//IsOdd retourne vrai si n est impair
func IsOdd(n Number) bool { return IsInteger(n) && Int(n).Bit(0) == 1 }

//Nan retourne un nombre non défini
func Nan() Number {
	return nb{
		d: new(big.Int),
		n: new(big.Int),
	}
}
func PositiveInfinite() Number {
	return nb{
		d: big.NewInt(1),
		n: new(big.Int),
	}
}
func NegativeInfinite() Number {
	return nb{
		d: big.NewInt(-1),
		n: new(big.Int),
	}
}
func Infinite(sign int) Number {
	if sign > 0 {
		return PositiveInfinite()
	} else if sign < 0 {
		return NegativeInfinite()
	}
	return Nan()
}

func Clone(n Number) Number {
	return nb{
		n: n.Numerator(),
		d: n.Denominator(),
	}
}
