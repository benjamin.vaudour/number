package number

import (
	"fmt"
	"math/big"
)

// Int représente un nombre entier implémentant l’interface Number.
type Int struct {
	atom[big.Int]
}

// INan retourne respectivement NaN, +∞ ou -∞
// suivant que sign est 0, 1, ou -1.
func INan(sign int, base ...int) (out Int) {
	out.setSign(sign)
	out.setBase(base...)
	return
}

// I retourne un entier suivant la valeur de n.
func I(n int64, base ...int) (out Int) {
	out.n, out.s, out.b = new(big.Int).SetInt64(n), 1, 10
	out.format(base...)
	return
}

func (i *Int) format(base ...int) Int {
	if i.IsDefined() {
		i.setSign(i.n.Sign() * i.s)
		if i.s == 0 {
			i.n.SetInt64(0)
		} else {
			i.n.Abs(i.n)
		}
	} else {
		i.setSign(i.s)
	}
	if len(base) > 0 {
		i.setBase(base...)
	}
	return *i
}

func (i Int) get() *big.Int {
	n := new(big.Int).Set(i.n)
	if i.s < 0 {
		return n.Neg(n)
	}
	return n
}

func (i Int) Type() NumberType {
	return INTEGER
}

func (i Int) ToInt(base ...int) (out Int) {
	out.s, out.b = i.s, i.b
	if i.IsDefined() {
		out.n = new(big.Int).Set(i.n)
	}
	out.format(base...)
	return
}

func (i Int) ToRat(base ...int) (out Rat) {
	out.s, out.b, out.t = i.s, i.b, DECIMAL
	if i.IsDefined() {
		out.n = new(big.Rat).SetInt(i.n)
	}
	return
}

func (i Int) String() string {
	if !i.IsDefined() {
		return undefStr(i.Sign())
	}
	s := ""
	if i.IsNeg() {
		s = "-"
	}
	b := i.Base()
	n := i.n.Text(b)
	if b != 10 {
		return fmt.Sprintf("(%d)%s%s", b, s, n)
	}
	return s + n
}

func (i Int) Neg() Int {
	out := i.ToInt()
	out.s = -out.s
	return out.format()
}

func (i Int) Abs() Int {
	out := i.ToInt()
	if out.s == 0 {
		return out
	}
	out.s = 1
	return out.format()
}

func (i1 Int) Cmp(i2 Int) int {
	d1, d2 := i1.IsDefined(), i2.IsDefined()
	s1, s2 := i1.Sign(), i2.Sign()
	switch {
	case !d1 && s1 == 0:
		if !d2 && s2 == 0 {
			return 0
		}
		return -2
	case s1 > s2:
		return 1
	case s1 < s2:
		return -1
	case !d1:
		if !d2 {
			return 0
		}
		return s1
	case !d2:
		return -s1
	case s1 >= 0:
		return i1.n.Cmp(i2.n)
	default:
		return i2.n.Cmp(i1.n)
	}
}

func (i1 Int) Eq(i2 Int) bool {
	return i1.Cmp(i2) == 0
}

func (i1 Int) Ne(i2 Int) bool {
	return i1.Cmp(i2) != 0
}

func (i1 Int) Gt(i2 Int) bool {
	return i1.Cmp(i2) == 1
}

func (i1 Int) Lt(i2 Int) bool {
	return i1.Cmp(i2) == -1
}

func (i1 Int) Ge(i2 Int) bool {
	c := i1.Cmp(i2)
	return c == 0 || c == 1
}

func (i1 Int) Le(i2 Int) bool {
	c := i1.Cmp(i2)
	return c == 0 || c == -1
}

func (i Int) Is(n int64) bool {
	if !i.IsDefined() {
		return false
	}
	in := i.get()
	return in.IsInt64() && in.Int64() == n
}

func (i1 Int) Add(i2 Int) Int {
	d1, d2 := i1.IsDefined(), i2.IsDefined()
	s1, s2 := i1.Sign(), i2.Sign()
	b := i1.Base()
	switch {
	case (!d1 && s1 == 0) || (!d2 && s2 == 0):
		return INan(0, b)
	case !d1 && !d2:
		if s1 != s2 {
			return INan(0, b)
		}
		return i1
	case !d1:
		return i1
	case !d2:
		return INan(s2, b)
	default:
		var out Int
		out.s, out.b = 1, b
		out.n = new(big.Int).Add(i1.get(), i2.get())
		return out.format()
	}
}

func (i1 Int) Sub(i2 Int) Int {
	d1, d2 := i1.IsDefined(), i2.IsDefined()
	s1, s2 := i1.Sign(), i2.Sign()
	b := i1.Base()
	switch {
	case (!d1 && s1 == 0) || (!d2 && s2 == 0):
		return INan(0, b)
	case !d1 && !d2:
		if s1 == s2 {
			return INan(0, b)
		}
		return i1
	case !d1:
		return i1
	case !d2:
		return INan(-s2, b)
	default:
		var out Int
		out.s, out.b = 1, b
		out.n = new(big.Int).Sub(i1.get(), i2.get())
		return out.format()
	}
}

func (i Int) Inc() Int {
	return i.Add(I(1))
}

func (i Int) Dec() Int {
	return i.Sub(I(1))
}

func (i1 Int) Mul(i2 Int) Int {
	s, b := i1.Sign()*i2.Sign(), i1.Base()
	if !i1.IsDefined() || !i2.IsDefined() {
		return INan(s, b)
	}
	var out Int
	out.s, out.b = s, b
	out.n = new(big.Int).Mul(i1.n, i2.n)
	return out.format()
}

func (i1 Int) Quo(i2 Int) Int {
	d1, d2 := i1.IsDefined(), i2.IsDefined()
	s1, s2 := i1.Sign(), i2.Sign()
	b := i1.Base()
	switch {
	case (!d1 && !d2) || (!d1 && s1 == 0) || (!d2 && s2 == 0) || (d1 && d2 && s1 == 0 && s2 == 0):
		return INan(0, b)
	case s2 == 0:
		return INan(s1, b)
	case s1 == 0:
		return I(0, b)
	case !d1:
		s := s1 * s2
		return INan(s, b)
	case !d2:
		return I(0, b)
	default:
		var out Int
		out.s, out.b = s1*s2, b
		out.n = new(big.Int).Quo(i1.n, i2.n)
		return out.format()
	}
}

func (i1 Int) Div(i2 Int) Rat {
	d1, d2 := i1.IsDefined(), i2.IsDefined()
	s1, s2 := i1.Sign(), i2.Sign()
	b := i1.Base()
	switch {
	case (!d1 && !d2) || (!d1 && s1 == 0) || (!d2 && s2 == 0) || (d1 && d2 && s1 == 0 && s2 == 0):
		return DNan(0, b)
	case s2 == 0:
		return DNan(s1, b)
	case s1 == 0:
		return DI(0, b)
	case !d1:
		s := s1 * s2
		return DNan(s, b)
	case !d2:
		return DI(0, b)
	default:
		var out Rat
		out.s, out.b, out.t = s1*s2, b, DECIMAL
		out.n = new(big.Rat).SetFrac(i1.n, i2.n)
		return out.format()
	}
}

func (i1 Int) Rem(i2 Int) Int {
	_, r := i1.QuoRem(i2)
	return r
}

func (i1 Int) QuoRem(i2 Int) (q, r Int) {
	q = i1.Quo(i2)
	r = i1.Sub(q.Mul(i2))
	return
}

func (i Int) IsEven() bool {
	return i.IsDefined() && i.n.Bit(0) == 0
}

func (i Int) IsOdd() bool {
	return i.IsDefined() && i.n.Bit(0) != 0
}

func (i1 Int) Lsh(i2 Int) Int {
	s, b := i1.Sign(), i1.Base()
	switch {
	case i1.IsNan() || i2.IsNan():
		return INan(0, b)
	case i1.IsZero() || i2.IsZero():
		return i1
	case i2.IsNeg():
		return i1.Rsh(i2.Neg())
	case !i1.IsDefined() || !i2.IsDefined() || !i2.n.IsUint64():
		return INan(i1.Sign(), b)
	default:
		var out Int
		out.b, out.s = b, s
		out.n = new(big.Int).Lsh(i1.n, uint(i2.n.Uint64()))
		return out.format()
	}
}

func (i1 Int) Rsh(i2 Int) Int {
	s, b := i1.Sign(), i1.Base()
	switch {
	case i1.IsNan() || i2.IsNan():
		return INan(0, b)
	case i1.IsZero() || i2.IsZero():
		return i1
	case i2.IsNeg():
		return i1.Lsh(i2.Neg())
	case !i2.IsDefined() || !i2.n.IsUint64():
		if !i1.IsDefined() {
			return INan(0, b)
		}
		return I(0, b)
	case !i1.IsDefined():
		return i1
	default:
		var out Int
		out.b, out.s = b, s
		out.n = new(big.Int).Rsh(i1.n, uint(i2.n.Uint64()))
		return out.format()
	}
}

func (i Int) Len() int {
	if !i.IsDefined() {
		return 0
	} else if i.IsZero() {
		return 1
	}
	s := new(big.Int).Abs(i.n).Text(i.b)
	return len(s)
}

func (i Int) Bit(b int) int64 {
	if b < 0 || !i.IsDefined() {
		return 0
	}
	base := i.Base()
	if b > 0 {
		i = i.Quo(pow(base, uint64(b)))
	}
	i = i.Rem(I(int64(base)))
	return i.n.Int64()
}

func (i1 Int) pow(i2 Int) Int {
	switch {
	case i2.IsZero():
		return I(1, i1.Base())
	case i2.Is(1):
		return i1
	case i2.Is(2):
		return i1.Mul(i1)
	case i2.IsEven():
		return i1.Mul(i1).Pow(i2.Rsh(I(1)))
	default:
		return i1.Mul(i1).Pow(i2.Rsh(I(1))).Mul(i1)
	}
}

func (i1 Int) Pow(i2 Int) Int {
	d1, d2 := i1.IsDefined(), i2.IsDefined()
	s1, s2 := i1.Sign(), i2.Sign()
	b := i1.Base()
	switch {
	case (!d1 && s1 == 0) || (!d2 && s2 == 0):
		return INan(0, b)
	case s2 < 0:
		if s1 == 0 {
			return INan(0, b)
		} else if i1.Is(1) {
			return i1
		}
		return I(0, b)
	case !d2:
		if s1 < 0 {
			return INan(0, b)
		}
		return INan(1, b)
	default:
		return i1.pow(i2)
	}
}

func (i Int) sqrt(t Int) Int {
	return (t.Add(i.Quo(t))).Quo(I(2))
}

func (i1 Int) sqrtn(i2, t Int) Int {
	n := i2.Dec()
	return ((t.Mul(n)).Add(i1.Quo(t.Pow(n)))).Quo(i2)
}

func (i Int) Sqrt() Int {
	b := i.Base()
	switch {
	case i.IsNeg() || i.IsNan():
		return INan(0, b)
	case !i.IsDefined() || i.Le(I(1)):
		return i
	default:
		out := i.sqrt(i)
		for {
			t := i.sqrt(out)
			if out.Eq(t) {
				break
			}
			out = t
		}
		return out
	}
}

func (i1 Int) Sqrtn(i2 Int) Int {
	b := i1.Base()
	switch {
	case i1.IsNan() || i2.IsNan() || i2.Lt(I(2)):
		return INan(0, b)
	case !i2.IsDefined():
		if i1.IsDefined() {
			return I(0, b)
		}
		return INan(0, b)
	case i1.IsNeg() && i2.IsEven():
		return INan(0, b)
	case i1.IsZero() || i1.Is(1) || !i1.IsDefined():
		return i1
	default:
		out := i1.sqrtn(i2, i1)
		for {
			t := i1.sqrtn(i2, out)
			if out.Eq(t) {
				break
			}
			out = t
		}
		return out
	}
}
