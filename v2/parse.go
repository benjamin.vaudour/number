package number

import (
	"fmt"
	"math/big"
	"regexp"
	"strconv"
	"strings"
)

const (
	rsign  = `(\+|-)`
	rsign2 = `(0|1)`
	rnb2   = rsign2
	rnb8   = `[0-7]`
	rnb10  = `\d`
	rnb16  = `[0-9a-fA-F]`
	rnbn   = `[0-9a-zA-Z]`
	rb2    = `B|b`
	rb8    = `O|o`
	rb16   = `X|x`
	rexp   = `E|e`
	rexp2  = `×\d+\^`
)

var (
	ri2  = fmt.Sprintf(`(%s)%s+`, rb2, rnb2)
	ri8  = fmt.Sprintf(`(%s)%s+`, rb8, rnb8)
	ri10 = fmt.Sprintf(`%s?%s+`, rsign, rnb10)
	ri16 = fmt.Sprintf(`(%s)%s+`, rb16, rnb16)
	rin  = fmt.Sprintf(`\(%s+\)%s?%s+`, rnb10, rsign, rnb10)
	rib  = fmt.Sprintf(`%s(%s|%s|%s)`, rsign2, ri2, ri8, ri16)
	rd2  = fmt.Sprintf(`(%s)(%s+\.%s*|\.%s+)`, rb2, rnb2, rnb2, rnb2)
	rd8  = fmt.Sprintf(`(%s)(%s+\.%s*|\.%s+)`, rb8, rnb8, rnb8, rnb8)
	rd10 = fmt.Sprintf(`%s?(%s+\.%s*|\.%s+)`, rsign, rnb10, rnb10, rnb10)
	rd16 = fmt.Sprintf(`(%s)(%s+\.%s*|\.%s+)`, rb16, rnb16, rnb16, rnb16)
	rdn  = fmt.Sprintf(`\(%s+\)%s?(%s+\.%s*|\.%s+)`, rnb10, rsign, rnbn, rnbn, rnbn)
	rdb  = fmt.Sprintf(`%s(%s|%s|%s)`, rsign2, rd2, rd8, rd16)
	re   = fmt.Sprintf(`(%s|%s)%s?%s+`, rexp, rexp2, rsign, rnb10)
	re2  = fmt.Sprintf(`%s?(%s)%s?%s+`, rsign, rexp, rsign, rnb10)
	rb   = fmt.Sprintf(`%s(%s|%s|%s)`, rsign2, rb2, rb8, rb16)
)

var (
	ri    = fmt.Sprintf(`%s|%s|%s`, ri10, rin, rib)
	rd    = fmt.Sprintf(`%s|%s|%s`, rd10, rdn, rdb)
	rint  = fmt.Sprintf(`^(%s)$`, ri)
	rdec  = fmt.Sprintf(`^(%s)$`, rd)
	rid   = fmt.Sprintf(`%s|%s`, ri, rd)
	rsci  = fmt.Sprintf(`^((%s)%s|%s)$`, rid, re, re2)
	rnb   = fmt.Sprintf(`(%s|(%s)%s|%s)`, rid, rid, re, re2)
	rfrac = fmt.Sprintf(`^%s/%s$`, rnb, rnb)
	rbase = fmt.Sprintf(`^%s.*`, rb)

	rk = map[byte]int{
		'X': 16,
		'x': 16,
		'O': 8,
		'o': 8,
		'B': 2,
		'b': 2,
	}

	snan = map[string]Number{
		"NaN":     INan(0),
		"+\u221E": INan(1),
		"-\u221E": INan(-1),
	}
)

func match(str string, reg string) bool {
	return regexp.MustCompile(reg).MatchString(str)
}

func isNanString(str string) bool {
	_, ok := snan[str]
	return ok
}

type parser struct {
	s  bool
	b  int
	be int
	e  int
	f  int
	n  string
}

func (p parser) String() string {
	return fmt.Sprintf(`{
  s:  %v,
  b:  %v,
  be: %v,
  e:  %v,
  f:  %v,
  n:  '%v',
}`, p.s, p.b, p.be, p.e, p.f, p.n)
}

func (p *parser) parseSign(str string) (next string, ok bool) {
	next = str
	if ok = len(str) > 0; ok {
		c := str[0]
		p.s = c == '-'
		if c == '-' || c == '+' {
			next = str[1:]
		}
	}
	return
}

func (p *parser) parseBaseSign(str string) (next string, ok bool) {
	next = str
	if ok = len(str) >= 2; ok {
		s, b := str[0], str[1]
		if ok = s == '0' || s == '1'; !ok {
			return
		}
		var bb int
		if bb, ok = rk[b]; ok {
			p.s, p.b = s == '1', bb
			next = str[2:]
		}
	}
	return
}

func (p *parser) parseBase(str string) (next string, ok bool) {
	next = str
	if ok = len(str) > 2 && str[0] == '('; ok {
		i := strings.IndexByte(str, ')')
		if ok = i > 1; ok {
			b, err := strconv.Atoi(str[1:i])
			if ok = err == nil && isBaseValid(b); ok {
				p.b, next = b, str[i+1:]
			}
		}
	}
	return
}

func (p *parser) parseFloat(str string) (next string, ok bool) {
	next = str
	i := strings.IndexByte(str, '.')
	if ok = i >= 0; ok {
		p.f = len(str) - 1 - i
		next = str[:i] + str[i+1:]
	}
	return
}

func (p *parser) parsePow(str string) (next string, ok bool) {
	next = str
	l := len(str)
	if ok = l >= 2; ok {
		i, j := strings.IndexRune(str, '×'), strings.IndexByte(str, '^')
		if ok = i > 0 && j > i+2 && l-i >= 5 && l-j >= 2; ok {
			b, err := strconv.Atoi(str[i+2 : j])
			if ok = err == nil && isBaseValid(b); ok {
				e, err := strconv.Atoi(str[j+1:])
				if ok = err == nil; ok {
					p.be, p.e = b, e
					next = str[:i]
				}
			}
		} else {
			str = strings.ToLower(str)
			i = strings.LastIndexByte(str, 'e')
			if ok = i >= 0 && i < l-1; ok {
				e, err := strconv.Atoi(str[i+1:])
				if ok = err == nil; ok {
					p.e = e
					next = next[:i]
				}
			}
		}
	}
	return
}

func (p *parser) parseInt(str string) (ok bool) {
	if match(str, rbase) {
		str, ok = p.parseBaseSign(str)
	} else {
		p.b = 10
		if len(str) > 0 && str[0] == '(' {
			str, ok = p.parseBase(str)
		} else {
			ok = true
		}
		if ok {
			str, ok = p.parseSign(str)
		}
	}
	if ok {
		p.n = str
	}
	return
}

func (p *parser) parseDec(str string) (ok bool) {
	if str, ok = p.parseFloat(str); ok {
		return p.parseInt(str)
	}
	return false
}

func (p *parser) parseSci(str string) (ok bool) {
	if str, ok = p.parsePow(str); ok {
		if str == "" || str == "+" || str == "-" {
			str += "1"
		}
		if match(str, rint) {
			ok = p.parseInt(str)
		} else {
			ok = p.parseDec(str)
		}
		if p.be == 0 {
			if ok = p.b < 14; ok {
				p.be = p.b
			}
		}
	}
	return
}

func (p parser) get() (n *big.Int, ok bool) {
	if n, ok = new(big.Int).SetString(p.n, p.b); ok {
		if p.s {
			n.Neg(n)
		}
	} else {
		n = nil
	}
	return
}

func isFracString(str string) (snum, sdenom string, ok bool) {
	if ok = strings.Count(str, "/") == 1; ok {
		frac := strings.IndexRune(str, '/')
		snum, sdenom = str[:frac], str[frac+1:]
	}
	return
}

func parseInt(str string) (n Int, ok bool) {
	var p parser
	if ok = p.parseInt(str); ok {
		if n.n, ok = p.get(); ok {
			n.s, n.b = 1, p.b
		}
	}
	n.format()
	return
}

func parseDec(str string) (n Rat, ok bool) {
	var p parser
	if ok = p.parseDec(str); ok {
		var num Int
		if num.n, ok = p.get(); ok {
			num.s, num.b = 1, p.b
			denom := pow(p.b, uint64(p.f))
			n = num.format().Div(denom)
		}
	}
	n.t = DECIMAL
	n.format()
	return
}

func parseSci(str string) (n Rat, ok bool) {
	var p parser
	if ok = p.parseSci(str); ok {
		var num Int
		if num.n, ok = p.get(); ok {
			num.s, num.b = 1, p.b
			denom := pow(p.b, uint64(p.f))
			n = num.format().Div(denom)
			if p.e < 0 {
				exp := pow(p.be, uint64(-p.e))
				n = n.Div(exp.ToRat())
			} else {
				exp := pow(p.be, uint64(p.e))
				n = n.Mul(exp.ToRat())
			}
		}
	}
	n.t = SCIENTIFIC
	n.format()
	return
}

func parseFracPart(str string) (n Rat, ok bool) {
	if match(str, rint) {
		var i Int
		if i, ok = parseInt(str); ok {
			n = i.ToRat().ToFrac()
			return
		}
	}
	if match(str, rdec) {
		if n, ok = parseDec(str); ok {
			n = n.ToFrac()
			return
		}
	}
	if match(str, rsci) {
		if n, ok = parseSci(str); ok {
			n = n.ToFrac()
			return
		}
	}
	n = FNan(0)
	return
}

func parseFrac(str string) (n Rat, ok bool) {
	i := strings.IndexByte(str, '/')
	if ok = i > 0 && i < len(str)-1; ok {
		snum, sdenom := str[:i], str[i+1:]
		var num, denom Rat
		if num, ok = parseFracPart(snum); ok {
			if denom, ok = parseFracPart(sdenom); ok {
				n = num.Div(denom)
				return
			}
		}
	}
	n = FNan(0)
	return
}

// Parse convertit la chaîne fournie en nombre.
// Si la conversion réusiit ok est vrai.
func Parse(str string) (n Number, ok bool) {
	if ok = isNanString(str); ok {
		n = snan[str]
		return
	}
	if match(str, rint) {
		if n, ok = parseInt(str); ok {
			return
		}
	}
	if match(str, rdec) {
		if n, ok = parseDec(str); ok {
			return
		}
	}
	if match(str, rsci) {
		if n, ok = parseSci(str); ok {
			return
		}
	}
	if match(str, rfrac) {
		if n, ok = parseFrac(str); ok {
			return
		}
	}
	n = INan(0)
	return
}

// ParseInt force la conversion en entier.
func ParseInt(str string, base ...int) (n Int, ok bool) {
	var nn Number
	if nn, ok = Parse(str); ok {
		n = nn.ToInt(base...)
	} else {
		n = INan(0)
	}
	return
}

// ParseRat force la conversionb en nombre rationnel.
func ParseRat(str string, base ...int) (n Rat, ok bool) {
	var nn Number
	if nn, ok = Parse(str); ok {
		n = nn.ToRat(base...)
	} else {
		n = DNan(0)
	}
	return
}

// ParseDec force la conversion en nombre décimal.
func ParseDec(str string, base ...int) (n Rat, ok bool) {
	n, ok = ParseRat(str, base...)
	n = n.ToDec()
	return
}

// ParseSci force la conversion en nombre scientifique.
func ParseSci(str string, base ...int) (n Rat, ok bool) {
	n, ok = ParseRat(str, base...)
	n = n.ToSci()
	return
}

// ParseFrac force la conversion en fraction.
func ParseFrac(str string, base ...int) (n Rat, ok bool) {
	n, ok = ParseRat(str, base...)
	n = n.ToFrac()
	return
}
