package number

import (
	"fmt"
	"math/big"
	"strings"
)

var (
	FloatingPrecision uint64 = 10    // Nombre de chiffres après la virgule pour un nombre décimal ou un nombre scientifique
	FixedPrecision           = false // Si vrai le nombre chiffre après la virgule est fixe.
)

// Rat représente un nombre non entier :
// - Soit une fraction, sous la forme n/d
// - Soit un décimal, sous la forme iiiii.dddddd
// - Soit un nombre scientifique, sous la forme 1.dddddEeee (ou 0.E-0, par exemple)
type Rat struct {
	atom[big.Rat]
	t NumberType
}

func newRat(t NumberType, s int, n *big.Rat, base ...int) (out Rat) {
	out.t, out.s, out.b, out.n = t, s, 10, n
	return out.format(base...)
}

func (r *Rat) format(base ...int) Rat {
	if r.IsDefined() {
		r.setSign(r.n.Sign() * r.s)
		if r.s == 0 {
			r.n.SetInt64(0)
		} else {
			r.n.Abs(r.n)
		}
	} else {
		r.setSign(r.s)
	}
	if len(base) > 0 {
		r.setBase(base...)
	}
	return *r
}

func (r Rat) get() *big.Rat {
	n := new(big.Rat).Set(r.n)
	if r.s < 0 {
		return n.Neg(n)
	}
	return n
}

func FNan(sign int, base ...int) Rat {
	return newRat(FRACTION, sign, nil, base...)
}

func FI(n int64, base ...int) Rat {
	return newRat(FRACTION, 1, big.NewRat(n, 1), base...)
}

func F(n, d int64, base ...int) Rat {
	if d == 0 {
		return FNan(int(n), base...)
	}
	return newRat(FRACTION, 1, big.NewRat(n, d), base...)
}

func DNan(sign int, base ...int) Rat {
	return newRat(DECIMAL, sign, nil, base...)
}

func DI(n int64, base ...int) Rat {
	return newRat(DECIMAL, 1, big.NewRat(n, 1), base...)
}

func D(n, d int64, base ...int) Rat {
	if d == 0 {
		return FNan(int(n), base...)
	}
	return newRat(DECIMAL, 1, big.NewRat(n, d), base...)
}

func SNan(sign int, base ...int) Rat {
	return newRat(SCIENTIFIC, sign, nil, base...)
}

func SI(n int64, base ...int) Rat {
	return newRat(SCIENTIFIC, 1, big.NewRat(n, 1), base...)
}

func S(n, d int64, base ...int) Rat {
	if d == 0 {
		return FNan(int(n), base...)
	}
	return newRat(SCIENTIFIC, 1, big.NewRat(n, d), base...)
}

func (r Rat) Type() NumberType {
	return r.t
}

func (r Rat) ToInt(base ...int) (out Int) {
	out.s, out.b = r.s, r.b
	if r.IsDefined() {
		out.n = new(big.Int).Quo(r.n.Num(), r.n.Denom())
	}
	return out.format(base...)
}

func (r Rat) ToRat(base ...int) (out Rat) {
	out.s, out.b, out.t = r.s, r.b, r.t
	if r.IsDefined() {
		out.n = new(big.Rat).Set(r.n)
	}
	return out.format(base...)
}

func (r *Rat) convert(t NumberType) Rat {
	r.t = t
	return *r
}

func (r Rat) ToFrac(base ...int) Rat {
	out := r.ToRat(base...)
	return out.convert(FRACTION)
}

func (r Rat) ToSci(base ...int) Rat {
	out := r.ToRat(base...)
	return out.convert(SCIENTIFIC)
}

func (r Rat) ToDec(base ...int) Rat {
	out := r.ToRat(base...)
	return out.convert(DECIMAL)
}

func (r Rat) Num() Int {
	if !r.IsDefined() {
		return INan(r.Sign(), r.Base())
	}
	var out Int
	out.b, out.s, out.n = r.b, r.s, r.n.Num()
	return out
}

func (r Rat) Denom() Int {
	if !r.IsDefined() {
		return I(1, r.Base())
	}
	var out Int
	out.b, out.s, out.n = r.b, 1, r.n.Denom()
	return out
}

func (r Rat) NumDenom() (n, d Int) {
	if !r.IsDefined() {
		return INan(r.Sign(), r.b), I(1, r.b)
	}
	n.b, n.s, n.n = r.b, r.s, r.n.Num()
	d.b, d.s, d.n = r.b, 1, r.n.Denom()
	return
}

func (r Rat) sFrac() string {
	n, d := r.NumDenom()
	return fmt.Sprintf("%s/%s", n, d)
}

func (r Rat) sDec() string {
	n, d := r.Abs().NumDenom()
	n = n.Mul(pow(r.b))
	q := n.Quo(d)
	s := "0"
	if n.Ge(d) {
		s = q.n.Text(r.b)
	}
	pp := int(FloatingPrecision)
	if len(s) < pp {
		s = strings.Repeat("0", pp) + s
	}
	dp := len(s) - pp
	s = fmt.Sprintf("%s.%s", s[:dp], s[dp:])
	if !FixedPrecision {
		l := len(s) - 1
		for s[l] == '0' {
			s, l = s[:l], l-1
		}
		if s == "." {
			s = "0."
		}
	}
	if r.IsNeg() {
		s = "-" + s
	} else if q.IsZero() && !r.IsZero() {
		s = "+" + s
	}
	if r.b == 10 {
		return s
	}
	return fmt.Sprintf("(%d)%s", r.b, s)
}

func (r Rat) sSci() string {
	var e int
	t := r
	if !r.IsZero() {
		n, d := r.Abs().NumDenom()
		nl, dl := n.Len(), d.Len()
		e = nl - dl
		if e > 0 {
			d = d.Mul(pow(r.b, uint64(e)))
		} else if e < 0 {
			n = n.Mul(pow(r.b, uint64(-e)))
		}
		if n.Lt(d) {
			e--
			n = n.Mul(pow(r.b, 1))
		}
		t = n.Div(d)
	}
	s := t.sDec()
	se := ""
	if e > 0 {
		se = "+"
	} else if e < 0 {
		se, e = "-", -e
	}
	if r.b == 10 {
		return fmt.Sprintf("%sE%s%d", s, se, e)
	}
	return fmt.Sprintf("%s×%d^%s%d", s, r.b, se, e)
}

func (r Rat) String() string {
	if !r.IsDefined() {
		return undefStr(r.Sign())
	}
	switch r.t {
	case FRACTION:
		return r.sFrac()
	case DECIMAL:
		return r.sDec()
	default:
		return r.sSci()
	}
}

func (r Rat) IsInt() bool {
	return r.Denom().Is(1)
}

func (r Rat) Neg() (out Rat) {
	out.b, out.s, out.t = r.b, -r.s, r.t
	if r.IsDefined() {
		out.n = new(big.Rat).Set(r.n)
	}
	return out.format()
}

func (r Rat) Abs() (out Rat) {
	out.b, out.s, out.t = r.b, 1, r.t
	if r.IsDefined() {
		out.n = new(big.Rat).Set(r.n)
	}
	return out.format()
}

func (r1 Rat) Cmp(r2 Rat) int {
	d1, d2 := r1.IsDefined(), r2.IsDefined()
	s1, s2 := r1.Sign(), r2.Sign()
	switch {
	case !d1 && s1 == 0:
		if !d2 && s2 == 0 {
			return 0
		}
		return -2
	case s1 > s2:
		return 1
	case s1 < s2:
		return -1
	case !d1:
		if !d2 {
			return 0
		}
		return s1
	case !d2:
		return -s1
	case s1 >= 0:
		return r1.n.Cmp(r2.n)
	default:
		return r2.n.Cmp(r1.n)
	}
}

func (r1 Rat) Eq(r2 Rat) bool {
	return r1.Cmp(r2) == 0
}

func (r1 Rat) Ne(r2 Rat) bool {
	return r1.Cmp(r2) != 0
}

func (r1 Rat) Gt(r2 Rat) bool {
	return r1.Cmp(r2) == 1
}

func (r1 Rat) Lt(r2 Rat) bool {
	return r1.Cmp(r2) == -1
}

func (r1 Rat) Ge(r2 Rat) bool {
	c := r1.Cmp(r2)
	return c == 0 || c == 1
}

func (r1 Rat) Le(r2 Rat) bool {
	c := r1.Cmp(r2)
	return c == 0 || c == -1
}

func (r Rat) Is(n int64) bool {
	if !r.IsDefined() || !r.IsInt() {
		return false
	}
	rn := r.get().Num()
	return rn.IsInt64() && rn.Int64() == n
}

func (r Rat) nan(s int) Rat {
	switch r.t {
	case FRACTION:
		return FNan(s, r.b)
	case DECIMAL:
		return DNan(s, r.b)
	default:
		return SNan(s, r.b)
	}
}

func (r Rat) int(n int64) Rat {
	switch r.t {
	case FRACTION:
		return FI(n, r.b)
	case DECIMAL:
		return DI(n, r.b)
	default:
		return SI(n, r.b)
	}
}

func (r Rat) frac(n, d int64) Rat {
	switch r.t {
	case FRACTION:
		return F(n, d, r.b)
	case DECIMAL:
		return D(n, d, r.b)
	default:
		return S(n, d, r.b)
	}
}

func (r1 Rat) Add(r2 Rat) Rat {
	d1, d2 := r1.IsDefined(), r2.IsDefined()
	s1, s2 := r1.Sign(), r2.Sign()
	switch {
	case (!d1 && s1 == 0) || (!d2 && s2 == 0):
		return r1.nan(0)
	case !d1 && !d2:
		if s1 != s2 {
			return r1.nan(0)
		}
		return r1
	case !d1:
		return r1
	case !d2:
		return r1.nan(s2)
	default:
		var out Rat
		out.s, out.b, out.t = 1, r1.b, r1.t
		out.n = new(big.Rat).Add(r1.get(), r2.get())
		return out.format()
	}
}

func (r1 Rat) Sub(r2 Rat) Rat {
	d1, d2 := r1.IsDefined(), r2.IsDefined()
	s1, s2 := r1.Sign(), r2.Sign()
	switch {
	case (!d1 && s1 == 0) || (!d2 && s2 == 0):
		return r1.nan(0)
	case !d1 && !d2:
		if s1 == s2 {
			return r1.nan(0)
		}
		return r1
	case !d1:
		return r1
	case !d2:
		return r1.nan(-s2)
	default:
		var out Rat
		out.s, out.b, out.t = 1, r1.b, r1.t
		out.n = new(big.Rat).Sub(r1.get(), r2.get())
		return out.format()
	}
}

func (r Rat) Inc() Rat {
	return r.Add(FI(1))
}

func (r Rat) Dec() Rat {
	return r.Sub(FI(1))
}

func (r1 Rat) Mul(r2 Rat) Rat {
	s := r1.Sign() * r2.Sign()
	if !r1.IsDefined() || !r2.IsDefined() {
		return r1.nan(s)
	}
	var out Rat
	out.s, out.b, out.t = s, r1.b, r1.t
	out.n = new(big.Rat).Mul(r1.n, r2.n)
	return out.format()
}

func (r1 Rat) Div(r2 Rat) Rat {
	d1, d2 := r1.IsDefined(), r2.IsDefined()
	s1, s2 := r1.Sign(), r2.Sign()
	switch {
	case (!d1 && !d2) || (!d1 && s1 == 0) || (!d2 && s2 == 0) || (d1 && d2 && s1 == 0 && s2 == 0):
		return r1.nan(0)
	case s2 == 0:
		return r1.nan(s1)
	case s1 == 0:
		return r1.int(0)
	case !d1:
		s := s1 * s2
		return r1.nan(s)
	case !d2:
		return r1.int(0)
	default:
		var out Rat
		out.s, out.b, out.t = s1*s2, r1.b, r1.t
		out.n = new(big.Rat).Quo(r1.n, r2.n)
		return out.format()
	}
}

func (r1 Rat) Quo(r2 Rat) Int {
	out := r1.Div(r2)
	return out.Num().Quo(out.Denom())
}

func (r1 Rat) Rem(r2 Rat) Rat {
	_, out := r1.QuoRem(r2)
	return out
}

func (r1 Rat) QuoRem(r2 Rat) (q Int, r Rat) {
	q = r1.Quo(r2)
	r = r1.Sub(q.ToRat().Mul(r2))
	return
}

func (r Rat) Inv() Rat {
	switch {
	case r.IsNan():
		return r
	case r.IsZero():
		return r.nan(1)
	case !r.IsDefined():
		return r.int(0)
	default:
		var out Rat
		out.b, out.s, out.t = r.b, r.s, r.t
		n, d := r.n.Num(), r.n.Denom()
		out.n = new(big.Rat).SetFrac(d, n)
		return out.format()
	}
}

func (r Rat) optimize(pb Int) Rat {
	if r.IsInt() {
		return r
	}
	p := pb.get()
	n1, d1 := r.n.Num(), r.n.Denom()
	t := new(big.Int).Mul(n1, p)
	t.Quo(t, d1)
	rn := new(big.Rat).SetFrac(t, p)
	n2, d2 := rn.Num(), rn.Denom()
	if n2.Cmp(n1) < 0 || d2.Cmp(d1) < 0 {
		return newRat(r.t, r.s, rn, r.b)
	}
	return r
}

func (r Rat) pow(n Int) Rat {
	d1, d2 := r.IsDefined(), n.IsDefined()
	s1, s2 := r.Sign(), n.Sign()
	switch {
	case (!d1 && s1 == 0) || (!d2 && s2 == 0):
		return r.nan(0)
	case !d2:
		if s1 < 0 {
			return r.nan(0)
		}
		return r.nan(1)
	case n.IsZero():
		return r.int(1)
	case n.Is(1):
		return r
	case n.Is(2):
		return r.Mul(r)
	case n.IsEven():
		return r.Mul(r).pow(n.Rsh(I(1)))
	default:
		return r.Mul(r).pow(n.Rsh(I(1))).Mul(r)
	}
}

func (r Rat) sqrt(t Rat) Rat {
	return (t.Add(r.Div(t))).Div(FI(2))
}

func (r Rat) sqrtn(s Int, t Rat) Rat {
	s1 := s.Dec()
	n, n1 := s.ToRat(), s1.ToRat()
	return ((t.Mul(n1)).Add(r.Div(t.pow(s1)))).Div(n)
}

func (r Rat) Sqrt() Rat {
	switch {
	case r.IsNeg() || r.IsNan():
		return r.nan(0)
	case !r.IsDefined() || r.IsZero() || r.Is(1):
		return r
	default:
		pb := pow(r.b)
		pi := pb.ToRat().Inv()
		out := r.sqrt(r).optimize(pb)
		for {
			t := r.sqrt(out).optimize(pb)
			if out.Eq(t) {
				break
			} else if (out.Sub(t)).Lt(pi) {
				out = t
				break
			}
			out = t
		}
		return out
	}
}

func (r Rat) Sqrtn(s Int) Rat {
	switch {
	case r.IsNan() || s.IsNan() || s.Lt(I(2)):
		return r.nan(0)
	case !s.IsDefined():
		if r.IsDefined() {
			return r.int(0)
		}
		return r.nan(0)
	case r.IsNeg() && s.IsEven():
		return r.nan(0)
	case r.IsZero() || r.Is(1) || !r.IsDefined():
		return r
	default:
		pb := pow(r.b)
		pi := pb.ToRat().Inv()
		out := r.sqrtn(s, r).optimize(pb)
		for {
			t := r.sqrtn(s, out).optimize(pb)
			if out.Eq(t) {
				break
			} else if ((out.Sub(t)).Abs()).Lt(pi) {
				out = t
				break
			}
			out = t
		}
		return out
	}
}

func (r1 Rat) Pow(r2 Rat) Rat {
	inv := r2.IsNeg()
	p, s := r2.Abs().NumDenom()
	out := r1.pow(p)
	if s.Gt(I(1)) {
		out = out.Sqrtn(s)
	}
	if inv {
		return out.Inv()
	}
	return out
}
