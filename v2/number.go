package number

// Nu.NumberType représente le type d’un nombre.
type NumberType int

const (
	INTEGER NumberType = iota
	DECIMAL
	FRACTION
	SCIENTIFIC
)

// Number est une interface représentant un nombre.
type Number interface {
	Type() NumberType // Type du nombre
	Sign() int        // Son signe (1 si positif, -1 si négatif, 0 si 0 ou NaN)
	Base() int        // Sa base (comprise entre 2 et 36)
	IsNeg() bool      // Vrai si c’est un nombre négatif
	IsPos() bool      // Vrai si c’est un nombre positif
	IsZero() bool     // Vrai si c’est le nombre 0
	IsDefined() bool  // Vrai si c’est un nombre non fini
	IsNan() bool      // Vrai si c’est NaN
	IsInfinite() bool // Vrai si c’est -∞ ou +∞
	IsInf() bool      // Vrai si c’est +∞
	IsNinf() bool     // Vrai si c’est -∞
	Is(int64) bool    // Vrai si le nombre égale n
	String() string   // Représentation graphique du nombre
	ToInt(...int) Int // Convertit le nombre en type entier
	ToRat(...int) Rat // Convertit le nombre en type non entier
}
