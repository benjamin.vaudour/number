package number

import (
	"math/big"
)

type atom[T big.Int | big.Rat] struct {
	b int
	s int
	n *T
}

func (a atom[T]) Sign() int {
	return a.s
}

func (a atom[T]) Base() int {
	return a.b
}

func (a atom[T]) IsDefined() bool {
	return a.n != nil
}

func (a atom[T]) IsNan() bool {
	return !a.IsDefined() && a.Sign() == 0
}

func (a atom[T]) IsInf() bool {
	return !a.IsDefined() && a.Sign() > 0
}

func (a atom[T]) IsNinf() bool {
	return !a.IsDefined() && a.Sign() < 0
}

func (a atom[T]) IsInfinite() bool {
	return !a.IsDefined() && a.Sign() != 0
}

func (a atom[T]) IsZero() bool {
	return a.IsDefined() && a.Sign() == 0
}

func (a atom[T]) IsNeg() bool {
	return a.Sign() < 0
}

func (a atom[T]) IsPos() bool {
	return a.Sign() > 0 && !a.IsZero()
}

func (a *atom[T]) setSign(s int) {
	if s > 0 {
		a.s = 1
	} else if s < 0 {
		a.s = -1
	} else {
		a.s = s
	}
}

func (a *atom[T]) setBase(base ...int) {
	b := 10
	if len(base) > 0 {
		b = base[0]
	}
	a.b = formatBase(b)
}
