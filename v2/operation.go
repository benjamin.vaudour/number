package number

// Nan retourne un nombre indéfini.
func Nan() Number {
	return INan(0)
}

// Inf retourne +∞.
func Inf() Number {
	return INan(1)
}

// Ninf retourne -∞.
func Ninf() Number {
	return INan(-1)
}

// Neg retourne -n.
func Neg(n Number) Number {
	if n.Type() == INTEGER {
		return n.ToInt().Neg()
	}
	return n.ToRat().Neg()
}

// Abs retourne |n|.
func Abs(n Number) Number {
	if n.Type() == INTEGER {
		return n.ToInt().Abs()
	}
	return n.ToRat().Abs()
}

// Inv retourne 1/n.
func Inv(n Number) Number {
	out := n.ToRat().Inv()
	if n.Type() == INTEGER && out.IsInt() || !out.IsDefined() {
		return out.ToInt()
	}
	return out
}

// Add retourne n1+n2.
func Add(n1, n2 Number) Number {
	t1, t2 := n1.Type(), n2.Type()
	if t1 == INTEGER && t2 == INTEGER {
		return n1.ToInt().Add(n2.ToInt())
	}
	out := n1.ToRat().Add(n2.ToRat())
	if t2 > t1 {
		return out.convert(t2)
	}
	return out
}

// Sub retourne n1-n2.
func Sub(n1, n2 Number) Number {
	t1, t2 := n1.Type(), n2.Type()
	if t1 == INTEGER && t2 == INTEGER {
		return n1.ToInt().Sub(n2.ToInt())
	}
	out := n1.ToRat().Sub(n2.ToRat())
	if t2 > t1 {
		return out.convert(t2)
	}
	return out
}

// Mul retourne n1×n2.
func Mul(n1, n2 Number) Number {
	t1, t2 := n1.Type(), n2.Type()
	if t1 == INTEGER && t2 == INTEGER {
		return n1.ToInt().Mul(n2.ToInt())
	}
	out := n1.ToRat().Mul(n2.ToRat())
	if t2 > t1 {
		return out.convert(t2)
	}
	return out
}

// Div retourne n1÷n2.
func Div(n1, n2 Number) Number {
	t1, t2 := n1.Type(), n2.Type()
	out := n1.ToRat().Div(n2.ToRat())
	if t1 == INTEGER && t2 == INTEGER && out.IsInt() {
		return out.ToInt()
	} else if t2 > t1 {
		return out.convert(t2)
	}
	return out
}

// Quo retourne la division entière de n1÷n2.
func Quo(n1, n2 Number) Number {
	t1, t2 := n1.Type(), n2.Type()
	if t1 == INTEGER && t2 == INTEGER {
		return n1.ToInt().Quo(n2.ToInt())
	}
	return n1.ToRat().Quo(n2.ToRat())
}

// Rem retourne  n1%n2.
func Rem(n1, n2 Number) Number {
	t1, t2 := n1.Type(), n2.Type()
	if t1 == INTEGER && t2 == INTEGER {
		return n1.ToInt().Rem(n2.ToInt())
	}
	return n1.ToRat().Rem(n2.ToRat())
}

// QuoRem retourne  n1/n2 & n1%n2.
func QuoRem(n1, n2 Number) (Number, Number) {
	t1, t2 := n1.Type(), n2.Type()
	if t1 == INTEGER && t2 == INTEGER {
		return n1.ToInt().QuoRem(n2.ToInt())
	}
	return n1.ToRat().QuoRem(n2.ToRat())
}

func _c(n1, n2 Number) int {
	return n1.ToRat().Cmp(n2.ToRat())
}

// Cmp compare n1 et n2.
func Cmp(n1, n2 Number) Number {
	return I(int64(_c(n1, n2)))
}

// Eq vérifie n1 = n2.
func Eq(n1, n2 Number) bool {
	return _c(n1, n2) == 0
}

// Ne vérifie n1 ≠ n2.
func Ne(n1, n2 Number) bool {
	return _c(n1, n2) != 0
}

// Gt vérifie n1 > n2.
func Gt(n1, n2 Number) bool {
	return _c(n1, n2) == 1
}

// Lt vérifie n1 < n2.
func Lt(n1, n2 Number) bool {
	return _c(n1, n2) == -1
}

// Ge vérifie n1 ≥ n2.
func Ge(n1, n2 Number) bool {
	c := _c(n1, n2)
	return c == 0 || c == 1
}

// Le vérifie n1 ≤ n2.
func Le(n1, n2 Number) bool {
	c := _c(n1, n2)
	return c == 0 || c == -1
}

// Num retourne le numérateur.
func Num(n Number) Number {
	return n.ToRat().Num()
}

// Denom retourne le dénominateur.
func Denom(n Number) Number {
	return n.ToRat().Denom()
}

// NumDenom retourne le numérateur et le dénominateur.
func NumDenom(n Number) (Number, Number) {
	return n.ToRat().NumDenom()
}

// Fact retourne n!.
func Fact(n Number) Number {
	if n.Sign() < 0 || n.Type() != INTEGER || !n.ToRat().IsInt() {
		return Nan()
	}
	if !n.IsDefined() {
		return INan(n.Sign())
	}
	result := I(1, n.Base())
	i := n.ToInt()
	for v := I(2); v.Le(i); v = v.Inc() {
		result = result.Mul(v)
	}
	return result
}

// Lsh retourne n1 << n2.
func Lsh(n1, n2 Number) Number {
	t1, t2 := n1.Type(), n2.Type()
	if t1 != INTEGER || t2 != INTEGER || !n1.ToRat().IsInt() || !n2.ToRat().IsInt() {
		return Nan()
	}
	return n1.ToInt().Lsh(n2.ToInt())
}

// Rsh retourne n1 >> n2.
func Rsh(n1, n2 Number) Number {
	t1, t2 := n1.Type(), n2.Type()
	if t1 != INTEGER || t2 != INTEGER || !n1.ToRat().IsInt() || !n2.ToRat().IsInt() {
		return Nan()
	}
	return n1.ToInt().Rsh(n2.ToInt())
}

// Sqrt retourne √n.
func Sqrt(n Number) Number {
	out := n.ToRat().Sqrt()
	if n.Type() == INTEGER && out.IsInt() {
		return out.ToInt()
	}
	return out
}

// Sqrtn retourne n^(1/s).
func Sqrtn(n, s Number) Number {
	if s.Type() != INTEGER || !s.ToRat().IsInt() {
		return Nan()
	}
	out := n.ToRat().Sqrtn(s.ToInt())
	if n.Type() == INTEGER && out.IsInt() {
		return out.ToInt()
	}
	return out
}

// Pow retourne n^p.
func Pow(n, p Number) Number {
	out := n.ToRat().Pow(p.ToRat())
	if n.Type() == INTEGER && out.IsInt() {
		return out.ToInt()
	}
	return out
}

// ToBase convertit n selon la base donnée.
func ToBase(n Number, b int) Number {
	if n.Type() == INTEGER {
		return n.ToInt(b)
	}
	return n.ToRat(b)
}

// ToInt convertit n en fraction.
// Si une base est donnée la base est également convertie.
func ToInt(n Number, base ...int) Number {
	return n.ToInt(base...)
}

// ToDec convertit n en nombre décimal.
// Si une base est donnée la base est également convertie.
func ToDec(n Number, base ...int) Number {
	return n.ToRat().ToDec(base...)
}

// ToFrac convertit n en fraction.
// Si une base est donnée la base est également convertie.
func ToFrac(n Number, base ...int) Number {
	return n.ToRat().ToFrac(base...)
}

// ToSci convertit n en nombre scientifique.
// Si une base est donnée la base est également convertie.
func ToSci(n Number, base ...int) Number {
	return n.ToRat().ToSci(base...)
}
