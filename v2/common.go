package number

func isBaseValid(b int) bool {
	return b >= 2 && b <= 36
}

func formatBase(b int) int {
	if isBaseValid(b) {
		return b
	}
	return 10
}

func undefStr(sign int) string {
	switch sign {
	case 0:
		return "NaN"
	case 1:
		return "+\u221E"
	default:
		return "-\u221E"
	}
}

func ipow(n Int, p uint64) Int {
	switch {
	case p == 0:
		return I(1)
	case p == 1:
		return n
	case p == 2:
		return n.Mul(n)
	case p&1 == 0:
		return ipow(n.Mul(n), p>>1)
	default:
		return ipow(n.Mul(n), p>>1).Mul(n)
	}
}

func pow(base int, precision ...uint64) Int {
	p := FloatingPrecision
	if len(precision) > 0 {
		p = precision[0]
	}
	return ipow(I(int64(base)), p)
}
