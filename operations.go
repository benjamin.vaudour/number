package number

import (
	"math/big"
)

func isBase(base int) bool {
	return base > 1 && base <= 63
}

func _base(base []int) int {
	if len(base) > 0 && isBase(base[0]) {
		return base[0]
	}
	return 10
}

//Format retourne le numérateur et de dénominateur
//formaté de sorte que le signe se trouve sur le numérateur
//et que le numérateur soit 0, 1 ou -1 si le nombre n’est pas réel.
func Format(n Number) (num, denom *big.Int) {
	num, denom = n.Numerator(), n.Denominator()
	if (denom.Sign()) == 0 {
		num = big.NewInt(int64(num.Sign()))
	} else {
		r := new(big.Rat).SetFrac(num, denom)
		num, denom = r.Num(), r.Denom()
	}
	return
}

//Neg retourne -n
func Neg(n Number) Number {
	n2 := nb{
		n: n.Numerator(),
		d: n.Denominator(),
	}
	n2.n.Neg(n2.n)
	return n2.format()
}

//Abs retourne |n|
func Abs(n Number) Number {
	n2 := nb{
		n: n.Numerator(),
		d: n.Denominator(),
	}
	n2.n.Abs(n2.n)
	n2.d.Abs(n2.d)
	return n2.format()
}

//Inv retourne 1/n
func Inv(n Number) Number {
	n2 := nb{
		n: n.Denominator(),
		d: n.Numerator(),
	}
	return n2.format()
}

//Add retourne n1 + n2
func Add(n1, n2 Number) Number {
	nn1, nn2 := !IsNumber(n1), !IsNumber(n2)
	switch {
	case nn1 && nn2 && SignOf(n1) != SignOf(n2):
		return Nan()
	case nn1:
		return n1
	case nn2:
		return n2
	default:
		r1, r2 := Rational(n1), Rational(n2)
		r3 := new(big.Rat).Add(r1, r2)
		return NewRational1(r3)
	}
}

//Sub retourne n1 - n2
func Sub(n1, n2 Number) Number { return Add(n1, Neg(n2)) }

//Mul retourne n1 × n2
func Mul(n1, n2 Number) Number {
	num := new(big.Int).Mul(n1.Numerator(), n2.Numerator())
	denom := new(big.Int).Mul(n1.Denominator(), n2.Denominator())
	return NewRational(num, denom)
}

//Div retourne n1 ÷ n2
func Div(n1, n2 Number) Number { return Mul(n1, Inv(n2)) }

//Cmp retourne la comparaison entre 2 nombres
func Cmp(n1, n2 Number) int {
	switch {
	case IsNan(n1):
		if IsNan(n2) {
			return 0
		}
		return -2
	case !IsNumber(n1):
		s := SignOf(n1)
		if IsNumber(n2) || SignOf(n2) != s {
			return s
		}
		return 0
	case !IsNumber(n2):
		return -SignOf(n2)
	default:
		return Rational(n1).Cmp(Rational(n2))
	}
}

func Eq(n1, n2 Number) bool { return Cmp(n1, n2) == 0 }
func Ne(n1, n2 Number) bool { return Cmp(n1, n2) != 0 }
func Gt(n1, n2 Number) bool { return Cmp(n1, n2) == 1 }
func Lt(n1, n2 Number) bool { return Cmp(n1, n2) == -1 }
func Ge(n1, n2 Number) bool { c := Cmp(n1, n2); return c == 0 || c == 1 }
func Le(n1, n2 Number) bool { c := Cmp(n1, n2); return c == 0 || c == -1 }

//Inc retourne n + 1
func Inc(n Number) Number { return Add(n, One()) }

//Dev retourne n - 1
func Dec(n Number) Number { return Sub(n, One()) }

//QuoRem retourne q et r de sorte que
//n1 = q × n2 + r
//avec q entier
func QuoRem(n1, n2 Number) (q Number, r Number) {
	q = Div(n1, n2)
	if !IsNumber(q) {
		r = Nan()
	} else {
		q = NewInteger(Int(q))
		r = Sub(n1, Mul(q, n2))
	}
	return
}

//Quo retourne la partie entière de la division
func Quo(n1, n2 Number) Number { q, _ := QuoRem(n1, n2); return q }

//Rem retourne n1 % n2
func Rem(n1, n2 Number) Number { _, r := QuoRem(n1, n2); return r }

//Fact retourne n!
func Fact(n Number) Number {
	if SignOf(n) < 0 || !IsInteger(n) {
		return Nan()
	}
	var v, result Number
	result = One()
	for v = Two(); Le(v, n); v = Inc(v) {
		result = Mul(result, v)
	}
	return result
}

func pow(n Number, p uint64) Number {
	switch {
	case p == 0:
		return One()
	case p == 1:
		return n
	case p == 2:
		return Mul(n, n)
	case p&1 == 0:
		return pow(Mul(n, n), p>>1)
	default:
		n2 := pow(Mul(n, n), p>>1)
		return Mul(n, n2)
	}
}

//BasePow retourne base ^ p
func BasePow(base int, p uint64) Number { return pow(NewInt(int64(base)), p) }

func formatShift(n Number) Number {
	if !IsNumber(n) || Int(n).IsUint64() {
		return n
	}
	if !IsInteger(n) {
		return Nan()
	}
	return PositiveInfinite()
}
func shift(n1, n2 Number) Number {
	negative, n2 := IsNegative(n2), formatShift(Abs(n2))
	switch {
	case IsNan(n1) || IsNan(n2):
		return Nan()
	case !IsNumber(n1) || Is(n1, 0):
		return n1
	case !IsNumber(n2):
		if negative {
			return Zero()
		}
		return Infinite(SignOf(n1))
	case IsInteger(n1):
		v1, v2 := Int(n1), uint(Int(n2).Uint64())
		v := new(big.Int)
		f := v.Lsh
		if negative {
			f = v.Rsh
		}
		return NewInteger(f(v1, v2))
	}
	p := BasePow(2, Int(n2).Uint64())
	if negative {
		p = Inv(p)
	}
	return Mul(n1, p)
}

//LSfhit retourne n1 << n2 si n2 > 0 et n1 >> n2 sinon
func LShift(n1, n2 Number) Number { return shift(n1, n2) }

//RSfhit retourne n1 >> n2 si n2 > 0 et n1 << n2 sinon
func RShift(n1, n2 Number) Number { return shift(n1, Neg(n2)) }

//Round arrondi le nombre à la précision souhaitée dans la base souhaitée
//(10 par défaut)
func Round(n Number, precision uint64, base ...int) Number {
	if !IsNumber(n) || (IsInteger(n)) {
		return n
	}
	b := _base(base)
	p := Int(BasePow(b, precision))
	r1, r2 := new(big.Int).Mul(n.Numerator(), p), n.Denominator()
	r := new(big.Rat).SetFrac(new(big.Int).Quo(r1, r2), p)
	return NewRational1(r)
}

func optimize(n Number, pp *big.Int) Number {
	if !IsNumber(n) || IsInteger(n) {
		return n
	}
	n1, n2 := n.Numerator(), n.Denominator()
	r1 := new(big.Int).Mul(n1, pp)
	r1.Quo(r1, n2)
	r := new(big.Rat).SetFrac(r1, pp)
	r1, r2 := new(big.Int).Abs(r.Num()), new(big.Int).Abs(r.Denom())
	if r1.Cmp(n1.Abs(n1)) < 0 || r2.Cmp(n2.Abs(n2)) < 0 {
		return NewRational1(r)
	}
	return n
}

func formatPow(n Number) (p, s Number) {
	p, s = n, Nan()
	switch {
	case !IsNumber(n):
	case IsInteger(n):
		if !Int(n).IsUint64() {
			p = PositiveInfinite()
		}
	default:
		v1, v2 := n.Numerator(), n.Denominator()
		if v1.IsUint64() {
			p = NewInteger(v1)
		} else {
			p = PositiveInfinite()
		}
		if v2.IsUint64() {
			s = NewInteger(v2)
		} else {
			s = PositiveInfinite()
		}
	}
	return
}

func sqrt(n, r Number, s uint64) Number {
	sn := NewUint(s)
	sn1 := NewUint(s - 1)
	return Div(Add(Mul(r, sn1), Div(n, pow(r, s-1))), sn)
}

//Sqrtn retourne n1 ^ (1/n2)
func Sqrtn(n1, n2 Number, precision int, base ...int) Number {
	switch {
	case SignOf(n1) < 0 || IsNan(n1) || IsNan(n2) || Lt(n2, Two()):
		return Nan()
	case !IsNumber(n1) || Is(n1, 0) || Is(n1, 1):
		return n1
	case !IsNumber(n2):
		return One()
	case !IsInteger(n2):
		return Nan()
	case !IsInteger(n2) || !Int(n2).IsUint64():
		return One()
	}
	if precision < 0 {
		precision = 10
	}
	b := _base(base)
	p := BasePow(b, uint64(precision))
	pp, min := Int(p), Inv(p)
	s := Int(n2).Uint64()
	result := optimize(sqrt(n1, n1, s), pp)
	for {
		r2 := optimize(sqrt(n1, result, s), pp)
		if Eq(result, r2) {
			break
		} else if Lt(Abs(Sub(result, r2)), min) {
			result = r2
			break
		}
		result = r2
	}
	return result
}

//Sqrt retourne √n
func Sqrt(n Number, precision int, base ...int) Number { return Sqrtn(n, Two(), precision, base...) }

//Pow retourne n1 ^ n2
func Pow(n1, n2 Number, precision int, base ...int) Number {
	if SignOf(n2) < 0 {
		return Inv(Pow(n1, Neg(n2), precision, base...))
	}
	p, s := formatPow(n2)
	switch {
	case IsNan(n1) || IsNan(p):
		return Nan()
	case Is(n1, 1) || Is(n1, 0):
		if Is(p, 0) {
			return One()
		}
		return n1
	case !IsNumber(p):
		if SignOf(n1) < 0 {
			return Nan()
		} else if Lt(n1, One()) {
			return Zero()
		}
		return PositiveInfinite()
	}
	result := pow(n1, Int(p).Uint64())
	if IsNan(result) || IsNan(s) {
		return result
	}
	return Sqrtn(result, s, precision, base...)
}

type Op1Func func(Number) Number
type Op2Func func(Number, Number) Number
