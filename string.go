package number

import (
	"fmt"
	"math/big"
	"regexp"
	"strconv"
	"strings"
)

func strNan(sign int) string {
	switch sign {
	case -1:
		return "-\u221E"
	case 1:
		return "+\u221E"
	default:
		return "<NaN>"
	}
}
func strPrefix(negative bool, base int) string {
	m := map[int]string{
		2:  "b",
		8:  "o",
		16: "x",
	}
	if p, ok := m[base]; ok {
		s := "0"
		if negative {
			s = "1"
		}
		return s + p
	}
	if base == 10 {
		if negative {
			return "-"
		}
		return ""
	}
	p := fmt.Sprintf("(%d)", base)
	if negative {
		return p + "-"
	}
	return p
}
func strInt(n *big.Int, negative bool, base int) string {
	return strPrefix(negative, base) + n.Text(base)
}
func strRat(n Number, base int) string {
	negative := IsNegative(n)
	n = Abs(n)
	r1, r2 := n.Numerator(), n.Denominator()
	s1 := strInt(r1, negative, base)
	s2 := strInt(r2, false, base)
	return fmt.Sprintf("%s/%s", s1, s2)
}
func strDec(n Number, base int, precision uint64, auto bool) string {
	negative := IsNegative(n)
	n = Abs(n)
	if Is(n, 0) {
		p := strPrefix(negative, base)
		s := "0."
		if !auto && precision > 0 {
			s = "." + strings.Repeat("0", int(precision))
		}
		return p + s
	}
	p := Int(BasePow(base, precision))
	r1, r2 := new(big.Int).Mul(p, n.Numerator()), n.Denominator()
	result := new(big.Rat).SetFrac(r1, r2)
	nbdec := int(precision)
	if auto && result.IsInt() {
		b := big.NewRat(1, int64(base))
		for nbdec > 0 {
			d := new(big.Rat).Mul(result, b)
			if !d.IsInt() {
				break
			}
			nbdec--
			result = d
		}
	}
	v := new(big.Int).Quo(result.Num(), result.Denom())
	s := v.Text(base)
	if l := len(s); nbdec > l {
		s = strings.Repeat("0", nbdec-l) + s
	}
	dot := len(s) - nbdec
	return fmt.Sprintf("%s%s.%s", strPrefix(negative, base), s[:dot], s[dot:])
}
func strExp(n Number, base int, precision uint64, auto bool) string {
	negative := IsNegative(n)
	if Is(n, 0) {
		p := strPrefix(negative, base)
		nb := "0."
		if !auto {
			nb = "." + strings.Repeat("0", int(precision))
		}
		b := "e+0"
		if base != 10 {
			b = fmt.Sprintf("×%d^+0", base)
		}
		return p + nb + b
	}
	result := Rational(Abs(n))
	s, e := "", 0
	if IsInteger(n) {
		s = Int(n).Text(base)
		e = len(s) - 1
	} else {
		r1, r2 := result.Num(), result.Denom()
		s1, s2 := r1.Text(base), r2.Text(base)
		e = len(s1) - len(s2)
		if e < 0 {
			s1 += strings.Repeat("0", -e)
		} else if e > 0 {
			s2 += strings.Repeat("0", e)
		}
		if s1 < s2 {
			e--
		}
		p := int(precision)
		if e < 0 {
			p -= e
		}
		if p > 0 {
			r1.Mul(r1, Int(BasePow(base, uint64(p))))
		}
		s = new(big.Int).Quo(r1, r2).Text(base)
	}
	if len(s) > int(precision)+1 {
		s = s[:int(precision)+1]
	}
	if auto {
		for l := len(s) - 1; l > 0 && s[l] == '0'; l-- {
			s = s[:l]
		}
	} else if d := int(precision) + 1 - len(s); d > 0 {
		s += strings.Repeat("0", d)
	}
	exp := "e"
	if base != 10 {
		exp = fmt.Sprintf("×%d^", base)
	}
	if negative {
		exp += "-"
	} else {
		exp += "+"
	}
	prefix := strPrefix(negative, base)
	return fmt.Sprintf("%s%s.%s%s%d", prefix, s[:1], s[1:], exp, e)
}

//IntString représente le nombre sous forme entière avec la base spécifiée
func IntString(n Number, base ...int) string {
	if !IsNumber(n) {
		return strNan(SignOf(n))
	}
	return strInt(Int(Abs(n)), IsNegative(n), _base(base))
}

//RationalString représente le nombre sous forme de fraction avec la base spécifiée
func RationalString(n Number, base ...int) string {
	if !IsNumber(n) {
		return strNan(SignOf(n))
	}
	return strRat(n, _base(base))
}

//DecimalString représente le nombre sous forme décimale avec la base spécifiée
//La précision indique le nombre chiffre après la virgule.
//Si auto, les zéro inutiles après la virgule sont tronqués.
func DecimalString(n Number, precision int, auto bool, base ...int) string {
	if !IsNumber(n) {
		return strNan(SignOf(n))
	}
	p := uint64(10)
	if precision >= 0 {
		p = uint64(precision)
	}
	return strDec(n, _base(base), p, auto)
}

//ScientificString représente le nombre sous forme scientifique avec la base spécifiée
//La précision indique le nombre chiffre après la virgule.
//Si auto, les zéro inutiles après la virgule sont tronqués.
func ScientificStringOf(n Number, precision int, auto bool, base ...int) string {
	if !IsNumber(n) {
		return strNan(SignOf(n))
	}
	p := uint64(10)
	if precision >= 0 {
		p = uint64(precision)
	}
	return strExp(n, _base(base), p, auto)
}

//StringOf représente le nombre sous forme entière ou décimale suivante
//que c’est un entier ou non
func StringOf(n Number, precision int, auto bool, base ...int) string {
	if auto && IsInteger(n) {
		return IntString(n, base...)
	}
	return DecimalString(n, precision, auto, base...)
}

func parseSign(s *string) (negative bool) {
	switch (*s)[0] {
	case '+':
		*s = (*s)[1:]
	case '-':
		*s, negative = (*s)[1:], true
	}
	return
}
func parseSignBase(s *string) (negative bool, base int) {
	negative = (*s)[0] == '1'
	switch (*s)[1] {
	case 'X', 'x':
		base = 16
	case 'O', 'o':
		base = 8
	case 'B', 'b':
		base = 2
	}
	*s = (*s)[2:]
	return
}
func parseFloat(s *string) (f int) {
	p := strings.IndexRune(*s, '.')
	if p >= 0 {
		f = len(*s) - 1 - p
		*s = (*s)[:p] + (*s)[p+1:]
	}
	return
}

func match(s string, r string) bool { return regexp.MustCompile(r).MatchString(s) }

func isIntString(s *string) (base int, negative, ok bool) {
	base, ok = 10, true
	switch {
	case match(*s, `^(\+|-)?\d+$`):
		negative = parseSign(s)
	case match(*s, `^(0|1)(X|x)[0-9A-Fa-f]+$`) || match(*s, `^(0|1)(B|b)(0|1)+$`) || match(*s, `^(0|1)(O|o)[0-7]+$`):
		negative, base = parseSignBase(s)
	case match(*s, `^\(\d+\)(\+|-)?[0-9A-Za-z]+$`):
		e := strings.IndexRune(*s, ')')
		base, _ = strconv.Atoi((*s)[1:e])
		if ok = isBase(base); ok {
			*s = (*s)[e+1:]
			negative = parseSign(s)
		}
	default:
		ok = false
	}
	return
}
func isDecString(s *string) (f, base int, negative, ok bool) {
	if strings.Count(*s, ".") != 1 {
		return
	}
	sc := *s
	f = parseFloat(&sc)
	if base, negative, ok = isIntString(&sc); ok {
		*s = sc
	}
	return
}
func isExpString(s *string) (e, f, base int, negative, ok bool) {
	if strings.Count(*s, "×") == 1 {
		i := strings.Index(*s, "×")
		sc, exp := (*s)[:i], (*s)[i+1:]
		if !match(exp, `^(\d+)\^(\+|-)?(\d+)$`) {
			return
		}
		ie := strings.Index(exp, "^")
		be, _ := strconv.Atoi(exp[:ie])
		e, _ = strconv.Atoi(exp[ie+1:])
		sci := sc
		if base, negative, ok = isIntString(&sci); ok {
			sc = sci
		} else {
			f, base, negative, ok = isDecString(&sc)
		}
		if ok = ok && base == be; ok {
			*s = sc
		}
		return
	}
	sc := strings.ToLower(*s)
	if strings.Count(sc, "e") != 1 {
		return
	}
	p := strings.IndexRune(sc, 'e')
	if !match(sc[p+1:], `^(\+|-)?\d+$`) {
		return
	}
	e, _ = strconv.Atoi(sc[p+1:])
	*s = sc[:p]
	if ok = len(*s) == 0 || (len(*s) == 1 && strings.Contains("+-", *s)); ok {
		negative, base = parseSign(s), 10
		*s = "1"
		return
	}
	sc = *s
	if base, negative, ok = isIntString(&sc); !ok {
		sc = *s
		f, base, negative, ok = isDecString(&sc)
	}
	if ok = ok && base == 10; ok {
		*s = sc
	}
	return
}
func isRatString(s string) (s1, s2 string, ok bool) {
	if ok = strings.Count(s, "/") == 1; ok {
		p := strings.Index(s, "/")
		s1, s2 = s[:p], s[p+1:]
	}
	return
}

func str2Int(s string, base int, negative bool) (r *big.Rat, ok bool) {
	var v *big.Int
	if v, ok = new(big.Int).SetString(s, base); ok {
		if negative {
			v.Neg(v)
		}
		r = new(big.Rat).SetInt(v)
	}
	return
}

type parseFunc func(string) (*big.Rat, int, bool)

func parseInt(s string) (r *big.Rat, base int, ok bool) {
	var negative bool
	if base, negative, ok = isIntString(&s); ok {
		r, ok = str2Int(s, base, negative)
	}
	return
}
func parseDec(s string) (r *big.Rat, base int, ok bool) {
	var negative bool
	var f int
	if f, base, negative, ok = isDecString(&s); ok {
		if r, ok = str2Int(s, base, negative); ok {
			p := Rational(BasePow(base, uint64(f)))
			r.Quo(r, p)
		}
	}
	return
}
func parseExp(s string) (r *big.Rat, base int, ok bool) {
	var negative bool
	var f, e int
	if e, f, base, negative, ok = isExpString(&s); ok {
		if r, ok = str2Int(s, base, negative); ok {
			d := e - f
			var p *big.Rat
			if d > 0 {
				p = Rational(BasePow(base, uint64(d)))
			} else if d < 0 {
				p = Rational(BasePow(base, uint64(-d)))
				p.Inv(p)
			}
			if p != nil {
				r.Mul(r, p)
			}
		}
	}
	return
}
func parseGuess(s string) (r *big.Rat, base int, ok bool) {
	for _, f := range []parseFunc{parseInt, parseDec, parseExp} {
		if r, base, ok = f(s); ok {
			break
		}
	}
	return
}
func parseRat(s string) (r *big.Rat, base, sign int, ok bool) {
	var s1, s2 string
	if s1, s2, ok = isRatString(s); !ok {
		return
	}
	var r1, r2 *big.Rat
	if r1, base, ok = parseGuess(s1); ok {
		if r2, _, ok = parseGuess(s2); ok {
			if r2.Sign() == 0 {
				sign = r1.Sign()
			} else {
				r = new(big.Rat).Quo(r1, r2)
			}
		}
	}
	return
}

//Parse convertit le paramètre fourni en nombre.
//Si la conversion a réussi, ok est vraie.
//La base retournée est celle reconnue dans le paramètre (10 par défaut)
func Parse(s string) (n Number, base int, ok bool) {
	for e := -1; e <= 1; e++ {
		nan := Infinite(e)
		if s == IntString(nan) {
			return nan, 10, true
		}
	}
	var r *big.Rat
	if r, base, ok = parseGuess(s); ok {
		n = NewRational1(r)
		return
	}
	var sign int
	if r, base, sign, ok = parseRat(s); ok {
		if r == nil {
			n = Infinite(sign)
		} else {
			n = NewRational1(r)
		}
	}
	return
}
