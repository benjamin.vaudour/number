package number

import (
	"math/big"
)

func NewRational(n, d *big.Int) Number {
	out := nb{
		n: n,
		d: d,
	}
	return out.format()
}

func NewRational1(r *big.Rat) Number {
	return NewRational(r.Num(), r.Denom())
}

func NewInteger(i *big.Int) Number { return NewRational(i, big.NewInt(1)) }

func NewFrac(n, d int64) Number { return NewRational(big.NewInt(n), big.NewInt(d)) }
func NewUfrac(n, d uint64) Number {
	return NewRational(new(big.Int).SetUint64(n), new(big.Int).SetUint64(d))
}

func NewInt(i int64) Number   { return NewFrac(i, 1) }
func NewUint(i uint64) Number { return NewUfrac(i, 1) }

func Zero() Number { return NewInt(0) }
func One() Number  { return NewInt(1) }
func Two() Number  { return NewInt(2) }

func Rational(n Number) *big.Rat {
	return new(big.Rat).SetFrac(n.Numerator(), n.Denominator())
}

func Int(n Number) *big.Int {
	return new(big.Int).Quo(n.Numerator(), n.Denominator())
}
